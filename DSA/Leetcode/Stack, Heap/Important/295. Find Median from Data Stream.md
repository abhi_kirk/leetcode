last attempt: `10/14/2023`

**Brute Force**
- Maintain a list of all elements. 
- When finding a median, sort the list, and get median by checking if length if even or odd. 
- Time Complexity:
  - `addNum()`: $O(1)$, 
  - `findMedian()`: $O(n\log n)$ due to sorting. 
- Space Complexity: $O(n)$. 

---

**Insertion Sort**
- *Intuition*: If the number of median calls are fewer than the insertion calls, then it will help if the list was always sorted when median needs to be computed. 
- Use insertion sort when inserting elements:
  - Scan the list and insert element when the next bigger element is found. 
- Time Complexity:
  - `addNum()`: $O(n)$ due to insertion sort.  
  - `findMedian()`: $O(1)$. 
- Space Complexity: $O(n)$. 

---

**Two Heaps**
- *Intuition*: To get rid of sorting, maintain a max-Heap with all lower elements, and a min-Heap with all higher elements. Then the top of the heaps will give us the median immediately. 
- When inserting an element, balance the heaps approximately. 
- Time Complexity:
  - `addNum()`: $O(\log n)$ since only pushing or popping from heaps. 
  - `findMedian()`: $O(1)$ since only checking tops of heaps.  
- Space Complexity: $O(n)$ since total length of two heaps is $n$. 

---

**Follow-Ups**
- If all elements are within [0, 100], use a frequency-count array where the array index represents the element. 
- If 99% of elements are within [0, 100], use two more frequency bins in addition to the above - one for <0 and another for >100 since we know that the median is still going to be within [0, 100]. 