**Two-pass**
- Iterate over the list with a counter, and find the length of the list. 
- Calculate index of node which needs to be removed. 
- Iterate over list again with a dummy head, and when the ignored node index is reached, skip over it and break. 
- Time Complexity: $O(n)$: Twice. 
- Space Complexity: $O(1)$.  