**Two Pointers**
- *Intuition*: Start with the full array. Let L be the leftmost element, R the rightmost.
  - If L + R = target: We're done
  - If L + R > target: The rightmost element is too big, because it exceeds the target, even when combined with the smallest available element. Since no element can be smaller than L, this proves that the rightmost element is useless and must be removed.
  - If L + R < target: The leftmost element is too small, because it is smaller than the target, even when combined with the biggest available element. Therefore it must be removed.
  - Once you removed the leftmost or the rightmost, the algorithm starts over on the remaining array.
- Time Complexity: $O(n)$. 
- Space Complexity: $O(1)$. 