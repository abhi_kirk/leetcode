**Binary Search on Array**
- Maintain hash map with `key` as the keys and a list of arrays `[t, value]` as values. 
- Since the list is already sorted, use the list directly in Binary Search for the `t` index. 