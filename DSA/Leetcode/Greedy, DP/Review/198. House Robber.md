last attempt: `10/15/2023`

**Bottoms-up DP**
- *Intuition*: Create an array (same size as `nums`) where each element represents the maximum amount that can be robbed up till that index.
- Each element at `i` follows the recurrence relation: max of   
  - element at `i` + maximum amount robbed till `i-2`, and
  - maximum amount robbed till `i-1`. 
- Base case is the first element of `nums` and the maximum of first and second elements. 
- Instead of actually creating the array, only keep track of previous two elements. 
- Time Complexity: $O(n)$. 
- Space Complexity: $O(1)$. 