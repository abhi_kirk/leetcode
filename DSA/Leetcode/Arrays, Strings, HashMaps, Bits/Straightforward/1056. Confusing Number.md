**Reverse then Invert**
- Create map with digits that are considered confusing. 
- Before returning `True` always check if the confused number is not equal to the original number. 
- Use concurrent division to get digits in a list in reverse order. 
- Iterate over this list and go through the map. Return if any digit is non-confusing. 
- Put the confused number back together, and compare with original. 
- Time Complexity: $O(n)$, where $n$ is the number of digits. 
- Space Complexity: $O(n)$. 

---
