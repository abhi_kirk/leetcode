# Case Studies Template
### *Step 1*: Project Scoping
- Start with defining *success* or goals of the new feature/product/study. 
- Then derive quantifiable metrics which tie in to the goals. 
  - Also derive *guardrail* metrics which protect from negative outcomes (declining quality from baseline, loss of revenue, etc.). 
- Stratify metrics across user segments, and time-periods. 
- Devise a strategy to evaluate the metrics - A/B testing, offline historical data analysis, or a canary release. 
  - Keep in mind MDE: Minimum Detectable Effect which comes from business requirements. 

### *Step 2*: Data Engineering
- What data sources do we need?
- Which platform hosts these data sources? On-prem, Cloud, RDBMS, NoSQL, GraphQL, etc. 
- How much data exists? Do we need to enable more data sources for this study?
- Is data structured? What kind of processing/cleaning has already been done, and what needs to be done?
- Batch vs. streaming data? Consider latency and its affects on users/metrics. 
  - Databases, API calls, or stream processing (e.g. Kafka). 

### *Step 3*: ML Model Development
- Preprocess the incoming data, including quantizing continuous variables for latency, converting categorical features to continuous if required, creating custom features, handling missing values, imbalanced classes. 
- Create embeddings (esp. from categorical features) if appropriate. Method used can be a simple stochastic gradient descent similar to the one used with matrix factorization. 
  - Creating embeddings: PCA/SVD, SGD, Word2Vec, CNNs. 
  - Computing embeddings offline allow us to simply use $O(1)$ lookup/hash tables at inference time. 
- Evaluation:
  - FPR, FNR, ROC (TPR vs. FPR for varying thresholds), k-fold cross-validation. 
  - Feature importance. 

### *Step 4*: Deployment

### *Step 5*: Monitoring and Continual Learning


# ML Use Cases
- Airbnb rental price prediction
- US Presidential election winner prediction
- Navigation for self-driving cars
- Email spam classification
- Netflix or Amazon recommender system
- App feature recommendations in a photo-enhancing app
- Matching fingerprints or face to person for authentication
- Language translator
- Speech recognition system
- At-home health monitoring system
- Credit card fraud detection
  - Learn the characteristics of good and bad transactions. 
  - Use up-sampling techniques. 
  - Tree-based algorithms work best, and are also explainable: isolation forests. 
- Price optimization for flight tickets
- Inventory optimization at a grocery store
- Customer acquisition by identifying potential customers at Lyft
- Churn prediction for customers
- Brand monitoring with public perception prediction
- Healthcare diagnosis
- Speed up customer service support by optimizing ticket routing
- Annotator matching
- Next app usage prediction