## DB Structure
- DB Software: MySQL, PostgreSQL, etc. 
- Database: Separate data entities with different commercial meaning. 
- Schema: Collection of Tables which can be split and grouped according to logic. 
  - Blueprints possible at this layer. 
- Table: Record of different data entities. 

<img src="../imgs/c1_database_structure.png" width=600>


## Filtering
- To include `NULL` values in filtering, use: `Col IS NULL` or `Col IS NOT NULL`. 
- The percent sign (`%`) will match zero, one, or multiple characters. To match exactly one character we could use an underscore (`_`). 
- Both `if` and `case` return a single value based on conditions given. 
  - `if` statement: `if(conditions, value_if_true, value_if_false)`. 
  - `case` statement:
    ```sql
    case when conditions
        then value_if_true
        else value_if_false
        end
    ```
- `where` clause is evaluated before `select` or `from` - hence any custom variables in `select` will not be identified by `where`. 
  - Solution: Use `having` instead of `where`. There is a performance hit from using `having` instead of `where`. 
  - `where` clause supports columns that already exist in the table. 

## Functions
- No Duplicates: `select distinct ...`
- Pagination:
  - Limit and Offset: `select * from Table limit x offset y`
    - `x`: how many records are displayed, 
    - `y`: number of rows to skip from top. 
- Sorting: `order by ColName ASC` or `order by ColName DESC`
  - Multi-column sorting when the rows for primary column have the same value: `order by ColName1 DESC, ColName2 ASC`
- Grouping: `group by ColName`
  - Count: `select count(*), ColName from TableName group by ColName`
- Sum: `sum(ColName)`
- Average: `avg(ColName)`
- Min, Max: `min(ColName)`
- String concatenation: `concat(ColName1, str, ColName2, ...) as DisplayCol`
  - This will return a single column, concatenating string values from specified columns. 

## Joins
- LEFT: 