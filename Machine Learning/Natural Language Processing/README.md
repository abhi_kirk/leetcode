**[spaCy](https://realpython.com/natural-language-processing-spacy-python/)**

spaCy contains models and data for various large language models, for different languages. 

Features for basic NLP:
- Tokenization, 
- Sentence detection with custom delimiters, 
- Checking attributes for token (alphanumeric, punctuation, etc.), 
- Stop words removal, 
- Lemmatization, 
- Word frequency (using `collections.Counter`), 
- Part-of-Speech (POS) tagging:
  - POS is a grammatical role that explains how a word is used in a sentence; e.g. Noun, Pronoun, Adjective, etc.
- Visualization for dependency parsing or named entities, 
- Rule-based matching for extracting information, 
- Extracting dependency graphs to represent its grammatical structure:
  - Words are nodes, and grammatical relationships are edge, 
  - Graph tree and subtree navigation. 
- Shallow parsing (also called chunking) to extract phrases from unstructured text, 
- Named-Entity Recognition (NER): process of locating named entities in unstructured text and then classifying them into predefined categories, such as person names, organization, locations, etc. 