# CNN Inspection Classifier

## Problem Statement
- Existing methodology

## Data Engineering
- Blob storage
- Data versioning

## Data Analytics
- Data labeling (semi-automated with increasingly complex classifiers) with org standardization
- Data balancing
  - Undersample prominent defect classes
  - Oversampling of infrequent defect class types with SMOTE
  - PoC on Generative Adversarial Networks (GANs) to enhance the dataset for undersampled classes
- Data augmentation
- Batch data processing in CNN, and with Stochastic Gradient Descent

## Model Development
- Pre-trained model selection with cross-validation
- Transfer learning
- Cold-start problem
- Focus on accuracy for certain classes
- Aggregating classes
- Cross-validation and hyper-parameter tuning
- Replacing fully connected network with Random Forest
- Including metadata into feature set before classification
- Traditional image processing for edge cases
- Image segmentation with RetinaNet
- Defect size classification
- *Anomaly detection* in images with OpenCV and CNN feature set
  - Blank and out-of-focus images
  - New types of defects
  - Labeling assistance
- Metrics: Precision, Recall, F-1 score, ROC curves, Confusion matrix

## Deployment
- Inference architecture
- Software wrapper for Tensorflow model
- TCP/IP communication with vendor lines
- Deployment on offline GPUs: performance and latency considerations
- *Root-Cause Aanalysis*: Feature visualization with image overlay

## Model Monitoring
- Metrics and monitoring with Windows Forms UI
- Human-in-the-loop validation for samples with low inference probabilities
- Data drift monitoring
- Infrastructure RCA
- Issue escalations

## Impact
- Automating deployments in a non-cloud environment